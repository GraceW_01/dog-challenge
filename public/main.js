//request1 represents the request for a random activity
var request1 = new XMLHttpRequest();
//request2 represents the request for an image of a dog
var request2 = new XMLHttpRequest();
//represents the activity the user will be suggested to perform
var suggestedActivity = "debug your code";
//represents an image of a dog
var dogImage;
//represents the breed of dog the user wishes to see photos of
var dogBreed = "dog";
var stepsCompleted = 0;

//takes the user's dog breed input and retrieves an image of that dog breed and a random activity
//from the locally hosted node.js server
function getSuggestionWithBreed()
{	
	stepsCompleted = 0;
	dogBreed = document.getElementById("userInput").value;
	request1.open('GET', 'http://localhost:3011/randomActivity');
	request1.send();
	request2.open('GET', 'http://localhost:3011/dogImage/' + dogBreed);
	request2.send();
	fetchingInProgress();
}

function getRandomSuggestion()
{
	stepsCompleted = 0;
	request1.open('GET', 'http://localhost:3011/randomActivity');
	request1.send();
	request2.open('GET', 'http://localhost:3011/randomDog');
	request2.send();
	fetchingInProgress();
}

request1.onload = () =>
{
	if (request1.status === 200)
	{
		stepsCompleted += 1;
		suggestedActivity = JSON.parse(request1.response).activity;
	}
	if (stepsCompleted == 2)
	{
		presentResults();
	}
}

request1.onerror = () => 
{
	console.log('activity could not be retrieved!');
	document.getElementById('errorLine').innerHTML = "<div id=t style=\"color:red\">please don't panic, but we couldn't find http://localhost:3011/randomActivity</div>";
}

request2.onload = () =>
{
	if (JSON.parse(request2.response).result === "success")
	{
		stepsCompleted += 1;
		dogImage = JSON.parse(request2.response).message;
	}
	else{
		dogBreedNotRecognized();
	}
	if (stepsCompleted == 2)
	{
		presentResults();
	}
}

request2.onerror = () => 
{
	console.log('dog image not be retrieved!');
	document.getElementById('errorLine').innerHTML = "<div id=t style=\"color:red\">please don't panic, but we couldn't find http://localhost:3011/dogImage/" + dogBreed + "</div>";
}

//slide a series of circles onto the screen, with theinformation recieved on them
function presentResults()
{
	document.getElementById('errorLine').innerHTML = "Complete!";
	document.getElementById('dismissCircle').className = "dismissCircleIn";
	document.getElementById('adviceCircle').className = "adviceCircleIn";
	document.getElementById('adviceCircle').innerHTML = suggestedActivity;
	document.getElementById('hintCircle').className = "hintCircleIn";
	document.getElementById('imageCircle').className = "imageCircleIn";
	document.getElementById('imageCircle').innerHTML = "<img class =\"dogPicture\"src=\"" + dogImage + "\">";
}

//slide a series of circles off the screen
function dismissSuggestion()
{
	stepsCompleted = 0;
	document.getElementById('errorLine').innerHTML = "";
	document.getElementById('dismissCircle').className = "dismissCircleOut";
	document.getElementById('adviceCircle').className = "adviceCircleOut";
	document.getElementById('hintCircle').className = "hintCircleOut";
	document.getElementById('imageCircle').className = "imageCircleOut";
}

//displays an error message in the error box
function dogBreedNotRecognized()
{	
	stepsCompleted = 0;
	let errorMessages = [
	"There's no way that's a real dog",
	"Are you certain you spelled that right?",
	"What kind of dog is that?",
	"You can't just... make up new dogs",
	"Sorry, we couldn't find a picture of a " + dogBreed,
	"Sorry, that kind of dog doesn't seem to exist.",
	"Sorry, the world just isn't ready for that dog yet.",
	"No photos found for " + dogBreed + ", sorry.",
	"I don't know what a " + dogBreed + " is, sorry.",
	"What's a " + dogBreed + "?",
	"No corresponding images found."]
	document.getElementById('errorLine').innerHTML = "<div id=t style=\"color:red\">" + errorMessages[Math.floor(Math.random() * 10)] + "</div>";
}

//lets the user know the process of data retireval has begun by placing a message in the error box
function fetchingInProgress()
{
	document.getElementById('errorLine').innerHTML = "<div id=t style=\"color:blue\">Fetching...</div>";
}










